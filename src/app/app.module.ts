import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';

import { AppComponent } from './app.component';
import { CountryListComponent } from './country-list/country-list.component';

import { DetailsComponent } from './details/details.component';
import { AppRoutingModule } from './app-routing.module';
import { CountryComponent } from './country-list/country/country.component';

@NgModule({
  declarations: [
    AppComponent,
    CountryListComponent,
    DetailsComponent,
    CountryComponent
  ],
  imports: [BrowserModule, HttpClientModule, AppRoutingModule],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {}
