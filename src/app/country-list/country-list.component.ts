import { Component, OnInit, OnDestroy } from '@angular/core';
import { Config } from '../country.interface';
import { HttpClient } from '@angular/common/http';
import { CountryService } from '../country.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-country-list',
  templateUrl: './country-list.component.html',
  styleUrls: ['./country-list.component.scss']
})
export class CountryListComponent implements OnInit, OnDestroy {
  config: any;
  subscription: Subscription;
  constructor(private countryService: CountryService) {}
  ngOnInit(): void {
    this.showConfig();

    console.log(this.config);
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }
  showConfig() {
    /*     this.countryService
      .getCountry()
      .subscribe((allCountry: Array<Config>) => (this.config = allCountry)); */

    this.config = this.countryService.getCountry();
    this.subscription = this.config.subscribe(a => console.log(a));
  }
}
