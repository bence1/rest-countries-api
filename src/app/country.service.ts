import { Injectable } from '@angular/core';
import { HttpClient, HttpRequest } from '@angular/common/http';
@Injectable({
  providedIn: 'root'
})
export class CountryService {
  private allCountry = 'https://restcountries.eu/rest/v2/all';
  constructor(private http: HttpClient) {}

  public getCountry() {
    return this.http.get(this.allCountry);
  }
}
