import { NgModule } from '@angular/core';
import { CountryListComponent } from './country-list/country-list.component';
import { DetailsComponent } from './details/details.component';
import { Routes, RouterModule } from '@angular/router';

const appRoutes: Routes = [
  { path: '', component: CountryListComponent },
  { path: 'details', component: DetailsComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(appRoutes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
